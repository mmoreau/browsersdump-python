# BrowsersDump-python

The project is under development => POC : Proof Of Concept

Retrieves information from various browsers such as Cookies, Browsing History, Bookmarks, Dom Storage, User Preferences and many others

## Warning / Remarks

* **MacOS** & **Linux** 
    * To be able to execute this script you must close the Firefox browser (kill process).

## Todo
### Browsers
* [ ] **Firefox** # (Windows 10 / Linux / MacOS)
* [ ] **Chrome**
* [ ] **Brave**
* [ ] **Opera**
* [ ] **Edge**

Try a way to find the default profile for Firefox