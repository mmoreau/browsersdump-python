#!/usr/bin/python3

import os.path
import sqlite3
import json
import platform as plf
import re
from subprocess import Popen, PIPE

try:
	import winreg as wr
except:
	pass

from datetime import datetime

class Firefox:

	@classmethod
	def profiles(cls, user):

		"""Retrieves the list of user profiles."""

		if isinstance(user, str):

			"""Path where all user profiles are located according to the operating system."""
			profiles = {
				"Windows": r"C:\Users\{}\AppData\Roaming\Mozilla\Firefox\Profiles".format(user),
				"Linux": r"/home/{}/.mozilla/firefox".format(user),
				"Darwin": r"/Users/{}/Library/Application Support/Firefox/Profiles".format(user)
			}

			OperatingSystem_Current = plf.system()

			if OperatingSystem_Current in profiles.keys():
				OperatingSystem_Profiles_Path = profiles[OperatingSystem_Current]

				if os.path.exists(OperatingSystem_Profiles_Path):
					return {"profiles": [os.path.join(OperatingSystem_Profiles_Path, elements) for elements in os.listdir(OperatingSystem_Profiles_Path) if ".".join(elements.split(".")[1:]) in ("default", "default-release")]}

		return {"profiles": None}



	@classmethod
	def addons(cls, profile_name):

		"""Retrieve the installed addons."""

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "addons.json")

			if os.path.exists(path):

				addons_data = []

				with open(path, "r", encoding="utf-8") as f:

					datas = json.load(f)

					for addons in datas["addons"]:

						keys = addons.keys()

						data = {
							"name": addons["name"],
							"version": addons["version"],
							"updateDate": datetime.fromtimestamp(addons["updateDate"] / 1e3),
							"reviewURL": addons["reviewURL"].split("reviews")[0],
							"sourceURI": addons["reviewURL"].split("reviews")[0]
						}

						if "homepageURL" in keys:
							if addons["homepageURL"]:
								data["homepageURL"] = addons["homepageURL"]

						addons_data.append(data)
					
				return {"addons": addons_data}
		
		return {"addons": []}



	@classmethod
	def bookmarks(cls, profile_name):

		"""Retrieves urls from favorite websites."""

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "places.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT
							bookmarks.title, 
							places.url, 
							datetime(bookmarks.dateAdded / 1000000, 'unixepoch'), 
							datetime(bookmarks.lastModified / 1000000 , 'unixepoch') 
						FROM moz_bookmarks bookmarks
						LEFT JOIN moz_places places
						ON bookmarks.fk = places.id
						WHERE places.url IS NOT NULL AND bookmarks.parent in (2, 3, 5)
						ORDER BY bookmarks.dateAdded DESC
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					bookmarks = []

					for row in rows:

						data = {
							"title": row[0],
							"url": row[1],
							"added": row[2],
							"modified": row[3]
						}

						bookmarks.append(data)

					return {"bookmarks": bookmarks}
				except:
					pass

		return {"bookmarks": []}



	@classmethod
	def history(cls, profile_name):

		"""Retrieves the history from the most recent to the oldest."""

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "places.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							title,
							url,
							description,
							datetime(last_visit_date / 1000000, 'unixepoch'),
							preview_image_url,
							visit_count
						FROM moz_places
						WHERE title IS NOT NULL AND last_visit_date IS NOT NULL
						ORDER BY last_visit_date DESC
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					history = []

					for row in rows:

						data = {
							"title": row[0],
							"url": row[1],
							"last_visit": row[3],
							"visit_count": row[5]
						}

						if row[2]:
							data["description"] = row[2]

						if row[4]:
							data["preview_image_url"] = row[4]

						history.append(data)

					return {"history": history}
				except:
					pass

		return {"history": []}



	@classmethod
	def cookies(cls, profile_name):

		"""Retrieves cookies stored while browsing websites."""

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "cookies.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							baseDomain,
							name, 
							value,
							host, 
							path,
							datetime(expiry, 'unixepoch'),
							datetime(lastAccessed / 1000000, 'unixepoch'),
							datetime(creationTime / 1000000, 'unixepoch'),
							isSecure,
							isHttpOnly,
							inBrowserElement,
							sameSite
						FROM moz_cookies
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					columns = [
						"baseDomain",
						"name", 
						"value",
						"host", 
						"path",
						"expiry",
						"lastAccessed",
						"creationTime",
						"isSecure",
						"isHttpOnly",
						"inBrowserElement",
						"sameSite"
					]

					cookies = []

					for row in rows:

						cookies_data = {}

						for data in enumerate(row):
							cookies_data[columns[data[0]]] = data[1]

						cookies.append(cookies_data)
						
					return {"cookies": cookies}	
				except:
					pass

		return {"cookies": []}



	@classmethod
	def domStorage(cls, profile_name):

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "webappsstore.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							originKey,
							scope,
							key, 
							value
						FROM webappsstore2
					"""
					cursor.execute(query)
					rows = cursor.fetchall()


					domstorage = []

					for row in rows:
						data = {
							"originKey": row[0],
							"scope": row[1],
							"key": row[2],
							"value": row[3]
						}

						domstorage.append(data)

					return {"domstorage": domstorage}
				except:
					pass

		return {"domstorage": []}



	@classmethod
	def formHistory(cls, profile_name):

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "formhistory.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							fieldname,
							value,
							datetime(firstUsed / 1000000, 'unixepoch'),
							datetime(lastUsed / 1000000, 'unixepoch')
						FROM moz_formhistory
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					formhistory = []

					for row in rows:

						data = {
							"fieldname": row[0],
							"value": row[1],
							"firstUsed": row[2],
							"lastUsed": row[3]
						}

						formhistory.append(data)

					return {"formhistory": formhistory}
				except:
					pass

		return {"formhistory": []}



	@classmethod
	def permissions(cls, profile_name):

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "permissions.sqlite")

			if os.path.exists(path):

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							origin,
							type,
							datetime(expireTime / 1000, 'unixepoch'),
							datetime(modificationTime / 1000, 'unixepoch')
						FROM moz_perms
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					permissions = []

					for row in rows:

						data = {
							"origin": row[0],
							"type": row[1],
							"expireTime": row[2],
							"modificationTime": row[3]
						}

						permissions.append(data)

					return {"permissions": permissions}
				except:
					pass

		return {"permissions": []}



	@classmethod
	def preferences(cls, profile_name):

		"""Returns only the list of modified configurations (about:config)."""

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "prefs.js")

			if os.path.exists(path):

				OperatingSystem_Current = plf.system()

				if OperatingSystem_Current == "Windows":

					preferences = []

					with open(path, "r") as f:
						for line in f.readlines():
							if line.startswith("user_pref"):
								preferences.append(line)

					return {"preferences": preferences}

		return {"preferences": []}



	@classmethod
	def isPrivate(cls, profile_name):

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "prefs.js")

			if os.path.exists(path):
				
				OperatingSystem_Current = plf.system()

				if OperatingSystem_Current == "Windows":
					return {"state": True if Popen("type {} | findstr \"browser.privatebrowsing.autostart\" 2> nul".format(path), shell=True, stdin=PIPE, stdout=PIPE, stderr=None).communicate()[0].decode() else False}
				elif OperatingSystem_Current in ("Linux", "Darwin"):
					return {"state": True if Popen("cat \'{}\' | egrep \"browser.privatebrowsing.autostart\" 2>/dev/null".format(path), shell=True, stdin=PIPE, stdout=PIPE, stderr=None).communicate()[0].decode() else False}
		
		return {"state": None}



	@classmethod
	def favicons(cls, profile_name):

		if isinstance(profile_name, str):

			path = os.path.join(profile_name, "favicons.sqlite")

			if os.path.exists(path):

				favicons = {}

				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					query = """
						SELECT 
							icon_url,
							width
						FROM moz_icons
					"""
					cursor.execute(query)
					rows = cursor.fetchall()

					icons = []

					for row in rows:
						if isinstance(row[0], str):
							if not row[0].startswith("fake-favicon-uri"):
								data = {
									"icon_url": row[0],
									"width": row[1]
								}
								
								icons.append(data)

					favicons["icons"] = icons
				except:
					pass


				try:
					conn = sqlite3.connect(path)
					cursor = conn.cursor()
					cursor.execute("SELECT page_url FROM moz_pages_w_icons")
					rows = cursor.fetchall()

					url = []

					for row in rows:
						for page_url in row:
							if isinstance(page_url, str):
								if not re.search("(support.mozilla.org|www.mozilla.org)", page_url) is not None:
									url.append(page_url)

					favicons["urls"] = url
				except:
					pass

				return {"favicons": favicons}

		return {"favicons": []}		



	@classmethod
	def version(cls):

		OperatingSystem_Current = plf.system()

		if OperatingSystem_Current == "Windows":
			try:
				with wr.OpenKey(wr.HKEY_CURRENT_USER, r"SOFTWARE\Mozilla\Mozilla Firefox") as hkey:
					return wr.QueryValueEx(hkey, "CurrentVersion")[0].split(" ")[0]
			except:
				pass
		elif OperatingSystem_Current == "Darwin":
			return Popen("/Applications/Firefox.app/Contents/MacOS/firefox -V | cut -d ' ' -f3 | tr -d '\n' 2>/dev/null", shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()[0].decode()

		elif OperatingSystem_Current == "Linux":
			return Popen("/usr/bin/firefox -V | cut -d ' ' -f3 | tr -d '\n' 2>/dev/null", shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()[0].decode()