#!/usr/bin/python3

import os
import platform as plf
import getpass
from Firefox import *

OperatingSystem_Current = plf.system()

if OperatingSystem_Current == "Windows":
	os.system("color A")

# .:: FIREFOX ::. #

# Put the name of the system user
getProfiles = Firefox.profiles(getpass.getuser())["profiles"]

if getProfiles:

	profile = getProfiles

	print(".:: Firefox Profiles ::.\n")

	for profile_list in profile:
		print("\t", profile_list) 


	# .:: BOOKMARKS ::. #

	"""
	getBookmarks = Firefox.bookmarks(profile)

	for bookmark in getBookmarks["bookmarks"]:
		for key, value in bookmark.items():
			print(key.capitalize().ljust(8), ":", value)
		
		print("\n" + "-"*100 + "\n")
	"""


	# .:: ADDONS ::. #

	"""
	getAddons = Firefox.addons(profile)

	for addons in getAddons["addons"]:
		for key, value in addons.items():
			print(key.capitalize().ljust(11), "=>", value)
		
		print("\n" + "-"*100 + "\n")
	"""



	# .:: COOKIES ::. #

	"""
	getCookies = Firefox.cookies(profile)

	for cookies in getCookies["cookies"]:
		for key, value in cookies.items():
			print(key.ljust(16), "=>", value, "\n", end="")

		print("\n" + "-"*100 + "\n")
	"""



	# .:: HISTORY ::. #

	"""
	getHistory = Firefox.history(profile)

	for history in getHistory["history"]:
		for key, value in history.items():
			print(key.capitalize().ljust(17), "=>", value)

		print("\n" + "-"*100 + "\n")
	"""



	# .:: DOM STORAGE ::. #

	"""
	getDomStorage = Firefox.domStorage(profile)

	for domstorage in getDomStorage["domstorage"]:
		for key, value in domstorage.items():
			print(key.ljust(16), "=>", value, "\n", end="")

		print("\n" + "-"*100 + "\n")
	"""



	# .:: FORM HISTORY ::. #

	"""
	getFormHistory = Firefox.formHistory(profile)

	for formhistory in getFormHistory["formhistory"]:
		for key, value in formhistory.items():
			print(key.ljust(9), "=>",  value, "\n", end="")

		print("\n" + "-"*100 + "\n")
	"""



	# .:: PERMISSIONS ::. #

	"""
	getPermissions = Firefox.permissions(profile)

	for permissions in getPermissions["permissions"]:
		for key, value in permissions.items():
			print(key.capitalize().ljust(16), "=>", value)

		print("\n" + "-"*100 + "\n")
	"""



	# .:: PREFERENCES ::. #

	"""
	getPreferences = Firefox.preferences(profile)

	for preferences in getPreferences["preferences"]:
		print(preferences)
	"""



	# .:: FAVICONS ::. #

	"""
	getFavicons = Firefox.favicons(profile)
	getFaviconsKeys = getFavicons["favicons"].keys()

	if "icons" in getFaviconsKeys:
		print(".:: FAVICONS ICONS ::.\n")
		for data in getFavicons["favicons"]["icons"]:
			for key, value in data.items():
				print("\t", key.ljust(8), "=>", value)

			print("")


	if "urls" in getFaviconsKeys:
		print("\n.:: FAVICONS URL ::.\n")
		for url in getFavicons["favicons"]["urls"]:
			print("\t", url)
	"""



	# .:: BROWSER PRIVATE ? ::. #

	"""
	getIsPrivate = Firefox.isPrivate(profile)

	if getIsPrivate:
		print("isPrivate :", getIsPrivate["state"])
	"""


# .:: VERSION ::. #

#print("\nFirefox Version :", Firefox.version())


if OperatingSystem_Current == "Windows":
	os.system("pause")